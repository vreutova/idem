========================
Idem for Microsoft Azure
========================

Setting up Microsoft Azure cloud resources using Idem is easy. See the documentation at the following link:

`Idem for Azure (idem-azurerm) Documentation
<https://idem-azurerm.readthedocs.io/en/latest/>`_
