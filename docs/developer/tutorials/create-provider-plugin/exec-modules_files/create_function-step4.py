async def create(hub, ctx, name, age, db_path="~/idem-sqlite-demo.db"):
    """Create a new database entry.

    Args:
        name (str):
            The person's name.

        age (int):
            The person's age.

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.create name="Sumit" age=35

        .. code-block:: bash

            $ idem exec demo.sqlite.create Sarah 36
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    cur = con.cursor()
    cur.execute("INSERT INTO people VALUES(null, ?, ?)", (name, age))
    con.commit()
    con.close()
    return result
