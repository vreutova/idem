.. idem documentation master file, created by
   sphinx-quickstart on Wed Feb 20 15:36:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _README:

.. include:: ../README.rst

.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: Using Idem

   topics/getting-started/index
   topics/tutorials/index
   topics/how-to/index
   topics/reference/index
   topics/explanation/index

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Developing for Idem

   developer/contributing/index
   developer/tutorials/index
   developer/how-to/index
   developer/reference/index
   developer/explanation/index

.. toctree::
   :hidden:
   :caption: Links

   Idem Website <https://www.idemproject.io>
   Idem Repository <https://gitlab.com/vmware/idem>
   Idem Project Docs <https://docs.idemproject.io>
   Download Idem <https://pypi.org/project/idem>
   topics/license
