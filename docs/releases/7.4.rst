========
Idem 7.4
========

Idem 7.4 adds the `tool` sub for dynamically loading
helper functions in idem projects.
