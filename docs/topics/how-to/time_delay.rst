==============
Sleep Function
==============

The idem time.sleep function pauses idem execution before or after resource state enforcement.
Use ``require`` to pause before enforcement or ``require_in`` to pause after enforcement.
Set the ``duration`` argument to the desired pause time in seconds.

The following example delays enforcement of the ``some_machine`` resource:

.. code-block:: yaml

    sleep_60s:
      time.sleep:
        - duration: 60

    some_machine:
      cloud.instance.present:
        - name: my-instance
        - require:
          - time.sleep: sleep_60s

The following example adds a delay after the ``some_machine`` resource is enforced:

.. code-block:: yaml

    sleep_60s:
      time.sleep:
        - duration: 60

    some_machine:
      cloud.instance.present:
        - name: my-instance
        - require_in:
          - time.sleep: sleep_60s
