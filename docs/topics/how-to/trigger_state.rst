==============
Trigger States
==============

A trigger state lets Idem define a dictionary of key-value (K-V) pairs. Keys are user-defined and values can be
output of any other state or any other value a user wishes to define.

Behavior of a trigger state:

* Check the value in the old_state dictionary and compare with the current_state.

* If there's a difference between old_state and new_state, result[changes] is populated with the difference.

A trigger state can be seen as a helper state. It can be used in conjunction with other states.

Example
=======

.. code-block:: sls

    always-changes-and-succeeds:
          test.succeed_with_changes:
            - name: foo

        always-changes-trigger:
            trigger.build:
                - triggers:
                     - last_run_id: {{ range(1, 51) | random }}
                     - comment: ${test:always-changes-and-succeeds:testing}

        watch_changes:
          test.nop:
            - onchanges:
              - trigger: always-changes-trigger


In the above example , ``always-changes-trigger`` is a trigger state with a dictionary of
K-V pairs. ``watch_changes`` is watching for ``changes`` of ``always-changes-trigger``.
Iff there are ``changes`` in the trigger state , state ``watch_changes`` will execute.
