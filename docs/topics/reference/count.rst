==================
Multiple Instances
==================

In an SLS file, a state can include a counting loop that allows you to create *n* number of instances of the same resource.

To create multiple instances, use names for state replication. The following example shows how to create four internet gateway resources that start with ``igw-`` in the name.

SLS

.. code-block:: sls

    igw-12345:
      aws.ec2.internet_gateway.present:
      - names:{% for i in range(4) %}
                Test {{ loop.index }}: {{loop.index}}
        {% endfor %}
      - tags:
        - Key: new-name
          Value: igw-9cd387e7

Result

.. code-block:: text

          ID: igw-12345
    Function: aws.ec2.internet_gateway.present
      Result: True
     Comment: Created 'igw-b440d01c'
     Changes: new:
        ----------
        Attachments:
        InternetGatewayId:
            igw-b440d01c
        OwnerId:
            000000000000
        Tags:
            |_
              ----------
              Key:
                  new-name
              Value:
                  igw-9cd387e7

.. code-block:: text

          ID: igw-12345
    Function: aws.ec2.internet_gateway.present
      Result: True
     Comment: Created 'igw-bc2861d1'
     Changes: new:
        ----------
        Attachments:
        InternetGatewayId:
            igw-bc2861d1
        OwnerId:
            000000000000
        Tags:
            |_
              ----------
              Key:
                  new-name
              Value:
                  igw-9cd387e7

.. code-block:: text

          ID: igw-12345
    Function: aws.ec2.internet_gateway.present
      Result: True
     Comment: Created 'igw-7bf9255d'
     Changes: new:
        ----------
        Attachments:
        InternetGatewayId:
            igw-7bf9255d
        OwnerId:
            000000000000
        Tags:
            |_
              ----------
              Key:
                  new-name
              Value:
                  igw-9cd387e7

.. code-block:: text

          ID: igw-12345
    Function: aws.ec2.internet_gateway.present
      Result: True
     Comment: Created 'igw-3aba95d8'
     Changes: new:
        ----------
        Attachments:
        InternetGatewayId:
            igw-3aba95d8
        OwnerId:
            000000000000
        Tags:
            |_
              ----------
              Key:
                  new-name
              Value:
                  igw-9cd387e7

To create states with different properties, use a jinja template in a loop. The following example shows how to create three subnets, where each subnet belongs to a different availability zone:

SLS

.. code-block:: sls

    {% set aws_availability_zones = {"available": { "names": ["us-east-2a", "us-west-2b", "eu-west-3"]}}%}
    {% set VpcSuperNet = "10.0."%}
    {% for i in range(3) %}
    aws_subnet.cluster-{{i}}:
      aws.ec2.subnet.present:
      - availability_zone: {{aws_availability_zones.available.names[i]}}
      - vpc_id: vpc-3d44da2d
      - cidr_block: {{VpcSuperNet+(i | string)}}.0/18
      - tags: {{ [{"Key":"Name", "Value":"test-"+(i | string)} ] }}
    {% endfor %}

Result

.. code-block:: text

          ID: aws_subnet.cluster-0
    Function: aws.ec2.subnet.present
      Result: True
     Comment: Created 'aws_subnet.cluster-0'
     Changes: new:
        ----------
        name:
            aws_subnet.cluster-0
        resource_id:
            subnet-d7cd43a1
        vpc_id:
            vpc-3d44da2d
        cidr_block:
            10.0.0.0/18
        availability_zone:
            us-west-2a
        tags:
            |_
              ----------
              Key:
                  Name
              Value:
                  test-0

.. code-block:: text

          ID: aws_subnet.cluster-1
    Function: aws.ec2.subnet.present
      Result: True
     Comment: Created 'aws_subnet.cluster-1'
     Changes: new:
        ----------
        name:
            aws_subnet.cluster-1
        resource_id:
            subnet-ad763648
        vpc_id:
            vpc-3d44da2d
        cidr_block:
            10.0.1.0/18
        availability_zone:
            us-west-2b
        tags:
            |_
              ----------
              Key:
                  Name
              Value:
                  test-0

.. code-block:: text

          ID: aws_subnet.cluster-2
    Function: aws.ec2.subnet.present
      Result: True
     Comment: Created 'aws_subnet.cluster-2'
     Changes: new:
        ----------
        name:
            aws_subnet.cluster-2
        resource_id:
            subnet-bc438686
        vpc_id:
            vpc-3d44da2d
        cidr_block:
            10.0.2.0/18
        availability_zone:
            us-west-2c
        tags:
            |_
              ----------
              Key:
                  Name
              Value:
                  test-0
