import unittest.mock as mock

import pop.hub
import pytest
import pytest_idem.runner as runner


@pytest.fixture(name="hub")
def hub_(tests_dir):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    runner.patch_hub(hub, tests_dir / "tpath")
    with mock.patch("sys.argv", ["state", "thing"]):
        hub.pop.config.load(hub.idem.CONFIG_LOAD, cli="idem", parse_cli=False)
    yield hub


@pytest.fixture(scope="module")
def acct_key():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="crypto")
    key = hub.crypto.fernet.generate_key()
    yield key
