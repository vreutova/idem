import tempfile


async def test_upgrade_version(hub):
    """
    Verify 2.0.0 ESM upgrade
    """
    cache_dir = tempfile.mkdtemp()

    async with hub.idem.managed.context("test", cache_dir) as ctx:
        # Put a low ESM version in the cache
        ctx[hub.idem.managed.ESM_METADATA_KEY] = {"version": [0, 0, 0]}
        ctx["test_|-unique_declaration_id_|-test_1_|-"] = {"resource_id": "resource123"}
        ctx["test_|-duplicate_declaration_id_|-test_1_|-"] = {
            "resource_id": "resource123"
        }
        ctx["test_|-duplicate_declaration_id_|-test_2_|-"] = {
            "resource_id": "resource123"
        }

    async with hub.idem.managed.context("test", cache_dir, upgrade_esm=True) as ctx:
        # The  version should have been bumped to the latest version
        assert (
            tuple(ctx[hub.idem.managed.ESM_METADATA_KEY]["version"]) == hub.esm.VERSION
        )

        # Upgraded to the new format
        assert ctx["test_|-unique_declaration_id_|-unique_declaration_id_|-"] == {
            "resource_id": "resource123"
        }
        assert ctx["test_|-duplicate_declaration_id_|-test_1_|-"] == {
            "resource_id": "resource123"
        }
        assert ctx["test_|-duplicate_declaration_id_|-test_2_|-"] == {
            "resource_id": "resource123"
        }
        assert "test_|-unique_declaration_id_|-test_1_|-" not in ctx
