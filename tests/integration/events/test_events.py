import json
import pathlib

import pytest
import pytest_idem.runner as runner


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-exec", "idem-*"], indirect=True)
async def test_exec(kafka, rabbitmq, event_acct_file, idem_cli):
    idem_cli(
        "exec",
        "test.ping",
        env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
        check=True,
    )

    expected = {
        "message": {"result": True, "ret": True},
        "tags": {"ref": "exec.test.ping", "type": "exec-post", "acct_details": None},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-low"], indirect=True)
async def test_low(kafka, rabbitmq, event_acct_file, idem_cli):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text("s:\n  test.succeed_without_changes")

        idem_cli(
            "state",
            str(fh),
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    expected = {
        "message": [
            {
                "__id__": "s",
                "__sls__": f".{fh.parent.name}.{fh.stem}",
                "fun": "succeed_without_changes",
                "name": "s",
                "order": 1,
                "state": "test",
            }
        ],
        "tags": {"ref": "idem.run.init.start", "type": "state-low-data"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-high"], indirect=True)
async def test_high(kafka, rabbitmq, event_acct_file, idem_cli):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text("s:\n  test.succeed_without_changes")

        idem_cli(
            "state",
            str(fh),
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    expected = {
        "message": {
            "s": {
                "__sls__": f".{fh.parent.name}.{fh.stem}",
                "test": ["succeed_without_changes"],
            }
        },
        "tags": {"ref": "idem.sls_source.init.gather", "type": "state-high-data"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-status"], indirect=True)
async def test_status(hub, kafka, rabbitmq, event_acct_file, idem_cli):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text("s:\n  test.succeed_without_changes")

        idem_cli(
            "state",
            str(fh),
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    await hub.pop.loop.sleep(2)

    for status in ("GATHERING", "COMPILING", "RUNNING", "FINISHED"):
        expected = {
            "message": status,
            "tags": {"ref": "idem.state.update_status", "type": "state-status"},
            "run_name": "cli",
        }

        # Test rabbitmq
        received_message = await rabbitmq.get()
        assert json.loads(received_message.body) == expected

        # Test Kafka
        received_message = await kafka.getone()
        assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-esm"], indirect=True)
async def test_esm_status(hub, kafka, rabbitmq, event_acct_file, idem_cli, tmpdir):
    global CACHE_DIR
    CACHE_DIR = pathlib.Path(tmpdir)
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text("s:\n  test.succeed_without_changes")

        idem_cli(
            "state",
            str(fh),
            "--esm-plugin=local",
            f"--cache-dir={CACHE_DIR / 'cache'}",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    await hub.pop.loop.sleep(2)

    expected = {
        "message": {"__esm_metadata__": {"version": list(hub.esm.VERSION)}},
        "tags": {"ref": "idem.state.update-status", "type": "esm-metadata"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-chunk"], indirect=True)
async def test_chunk(
    hub, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx, idem_cli
):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text("s:\n  test.succeed_with_comment:\n    - comment: asdf")

        idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    expected = {
        "message": {
            "__id__": "s",
            "__sls__": f".{fh.parent.name}.{fh.stem}",
            "comment": "asdf",
            "fun": "succeed_with_comment",
            "name": "s",
            "state": "test",
            "order": 100000,
        },
        "tags": {
            "ref": "states.test.succeed_with_comment",
            "type": "state-chunk",
        },
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-state"], indirect=True)
async def test_state_pre_post(
    hub, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx, idem_cli
):
    acct_data = {"profiles": {"test": {"default": {"account_id": 1234}}}}

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            f"s:\n  test.succeed_without_changes:\n    - acct_data: {json.dumps(acct_data)}"
        )

        idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    expected_pre = {
        "message": {
            "s": {
                "test.succeed_without_changes": {
                    "ctx": {
                        "acct_details": {"account_id": 1234},
                        "old_state": None,
                        "rerun_data": None,
                        "run_name": "cli",
                        "test": False,
                        "tag": "test_|-s_|-s_|-succeed_without_changes",
                    },
                    "kwargs": {},
                    "name": "s",
                }
            }
        },
        "run_name": "cli",
        "tags": {
            "ref": "states.test.succeed_without_changes",
            "type": "state-pre",
            "acct_details": {"account_id": 1234},
        },
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_pre

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_pre

    expected_post = {
        "message": {
            "changes": {},
            "comment": "Success!",
            "name": "s",
            "result": True,
        },
        "tags": {
            "ref": "states.test.succeed_without_changes",
            "type": "state-post",
            "acct_details": {"account_id": 1234},
        },
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_post


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-state"], indirect=True)
async def test_state_exec_pre_post(
    hub, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx, idem_cli
):
    acct_data = {"profiles": {"test": {"default": {"account_id": 1234}}}}

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            f"s:\n  exec.run:\n    - acct_data: {json.dumps(acct_data)}\n    - path: test.ping"
        )

        idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    expected_pre = {
        "message": {"s": {"test.ping": {}}},
        "run_name": "cli",
        "tags": {
            "ref": "exec.test.ping",
            "type": "state-pre",
            "acct_details": {"account_id": 1234},
        },
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_pre

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_pre

    expected_post = {
        "message": {
            "changes": {"new": True},
            "comment": ["idem exec test.ping --acct-profile=default"],
            "name": "s",
            "new_state": True,
            "old_state": None,
            "result": True,
        },
        "tags": {
            "ref": "exec.test.ping",
            "type": "state-post",
            "acct_details": {"account_id": 1234},
        },
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_post


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-run"], indirect=True)
async def test_state_run(
    hub, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx, idem_cli
):
    acct_data = {"profiles": {"test": {"default": {"account_id": 1234}}}}

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            f"s:\n  test.succeed_without_changes:\n    - acct_data: {json.dumps(acct_data)}"
        )

        idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            env={"ACCT_KEY": "", "ACCT_FILE": event_acct_file},
            check=True,
        )

    expected_post = {
        "message": {
            "__id__": "s",
            "__run_num": 1,
            "changes": {},
            "comment": "Success!",
            "esm_tag": "test_|-s_|-s_|-",
            "name": "s",
            "new_state": None,
            "old_state": None,
            "rerun_data": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "tag": "test_|-s_|-s_|-succeed_without_changes",
        },
        "run_name": "cli",
        "tags": {
            "ref": "states.test.succeed_without_changes",
            "type": "state-result",
            "acct_details": {},
        },
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    data = json.loads(received_message.body)
    # We can't mock these values in subprocess call, it's good enough that they can be popped
    data["message"].pop("start_time")
    data["message"].pop("total_seconds")
    assert data == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    data = json.loads(received_message.value)
    # We can't mock these values in subprocess call, it's good enough that they can be popped
    data["message"].pop("start_time")
    data["message"].pop("total_seconds")
    assert data == expected_post
