import pathlib
import shutil

import msgpack


def test_missing_required_params(idem_cli, tests_dir):
    # Test that function is not called if required param/s are missing
    ret = idem_cli("state", tests_dir / "sls" / "missing_params.sls")

    assert ret.result
    assert ret.json["test_|-test_missing_params_|-test_missing_params_|-required"]
    assert (
        ret.json["test_|-test_missing_params_|-test_missing_params_|-required"][
            "result"
        ]
        is False
    )
    assert (
        "states.test.required: ValueError: states.test.required is missing required argument(s): required_param"
        in ret.json["test_|-test_missing_params_|-test_missing_params_|-required"][
            "comment"
        ]
    )


def test_invalid_bool_params(idem_cli, tests_dir):
    # Test that function is not called if a boolean type param is a non boolean value
    ret = idem_cli("state", tests_dir / "sls" / "invalid_param.sls")

    assert ret.result
    assert ret.json[
        "test_|-test_invalid_bool_expected_|-test_invalid_bool_expected_|-required"
    ]
    assert (
        ret.json[
            "test_|-test_invalid_bool_expected_|-test_invalid_bool_expected_|-required"
        ]["result"]
        is False
    )

    assert (
        "states.test.required: TypeError: states.test.required is expecting a boolean value for 'bool_param' but got 'not_a_boolean_value'"
        in ret.json[
            "test_|-test_invalid_bool_expected_|-test_invalid_bool_expected_|-required"
        ]["comment"]
    )


def test_partial_success_with_esm(idem_cli, tests_dir, tmpdir):
    """
    Make sure successful resource execution is stored in ESM while other failed
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        ret = idem_cli(
            "state",
            tests_dir / "sls" / "missing_params_with_partial_success.sls",
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
        )

        assert esm_cache.exists()

        assert ret.json["test_|-passing_state_|-passing_state_|-present"]
        successful_resource = ret.json["test_|-passing_state_|-passing_state_|-present"]
        assert successful_resource["result"] is True

        assert ret.json["test_|-test_missing_params_|-test_missing_params_|-required"]
        failed_resource = ret.json[
            "test_|-test_missing_params_|-test_missing_params_|-required"
        ]

        assert failed_resource["result"] is False

        assert (
            "states.test.required: ValueError: states.test.required is missing required argument(s): required_param"
            in failed_resource["comment"]
        )

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            metadata = data.pop("__esm_metadata__")
            assert metadata["version"]
            # passing state should have been saved in ESM
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"
