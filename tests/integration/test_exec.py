import subprocess
import sys

from pytest_idem.runner import run_ex

from idem.exec.init import ExecReturn


def test_shell_exec(runpy):
    cmd = [sys.executable, runpy, "exec", "test.ping", "--output=exec"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert ret.returncode == 0
    assert b"True" in ret.stdout


def test_shell_exec_fail(runpy):
    cmd = [sys.executable, runpy, "exec", "test.ping", "extra_arg", "--output=exec"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    assert ret.returncode == 1


def test_default_profile(code_dir, hub):
    acct_fn = code_dir.joinpath("tests", "files", "acct")
    ret = run_ex("tst_ctx.get", [], {}, acct_file=acct_fn)
    assert ret == ExecReturn(
        result=True,
        comment=None,
        ret={
            "acct": {"foo": "bar"},
            "acct_details": {"account_id": None},
            "test": False,
        },
        ref="exec.tst_ctx.get",
    )


def test_profile(code_dir, hub):
    acct_fn = code_dir / "tests" / "files" / "acct"
    ret = run_ex(
        "tst_ctx.get",
        [],
        {},
        acct_file=acct_fn,
        acct_profile="extra",
    )
    assert ret == ExecReturn(
        result=True,
        comment=None,
        ret={
            "acct": {"quo": "qux"},
            "acct_details": {"account_id": None},
            "test": False,
        },
        ref="exec.tst_ctx.get",
    )


def test_exec_namespacedict(code_dir, hub):
    # I'm not writing a contract to ensure that ctx is always converted
    # to a NamespaceDict because idem is responsible for creating ctx and
    # its type directly.
    acct_fn = code_dir.joinpath("tests", "files", "acct")
    ret = run_ex("tests.ctx.assert_ctx_type", [], {}, acct_file=acct_fn)
    assert ret["result"] is True, "comment"


def test_ping(hub):
    ret = hub.exec.test.ping()
    assert ret == ExecReturn(result=True, ref="exec.test.ping", ret=True, comment=None)
    assert isinstance(ret, ExecReturn)
    assert bool(ret) is ret.result
    assert ret.result is ret["result"]
    assert ret.comment is ret["comment"]
    assert ret.ref is ret["ref"]
    return ret


async def test_aping(hub, event_loop):
    ret = await hub.exec.test.aping()
    assert ret == ExecReturn(result=True, ref="exec.test.aping", ret=True, comment=None)
    assert isinstance(ret, ExecReturn)
    assert bool(ret) is ret.result
    assert ret.result is ret["result"]
    assert ret.comment is ret["comment"]
    assert ret.ref is ret["ref"]
    return ret


def test_failure(hub):
    ret = run_ex("test.fail", [], {})
    assert ret.result is False
    assert ret.ret is None
    assert ret.comment == ["Exception: Expected failure"]
    assert ret.ref == "exec.test.fail"


def test_asyncfailure(hub):
    ret = run_ex("test.afail", [], {})
    assert ret.result is False
    assert ret.ret is None
    assert ret.comment == ["Exception: Expected failure"]
    assert ret.ref == "exec.test.afail"


def test_echo(idem_cli):
    ret = idem_cli("exec", "test.echo", "taco")
    assert ret.json == {
        "result": True,
        "ret": "taco",
        "comment": None,
        "ref": "exec.test.echo",
    }


def test_aecho(idem_cli):
    ret = idem_cli("exec", "test.aecho", "taco")
    assert ret.json == {
        "result": True,
        "ret": "taco",
        "comment": None,
        "ref": "exec.test.aecho",
    }
