from typing import Dict

import pytest
from pytest_idem.runner import IdemRunException
from pytest_idem.runner import run_sls


def test_params():
    run_ret = run_sls(["success"], params=["params"], sls_offset="sls/params")
    assert run_ret
    assert isinstance(run_ret, dict), run_ret
    assert len(run_ret) == 1, "Expecting 1 state"
    for state, ret in run_ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["location"] == "eastus"
        assert ret["new_state"]["backup_location"] == "westus"
        assert ret["new_state"]["empty"] is None
        assert ret["new_state"]["empty_str"] == "_str"
        assert ret["new_state"]["include_1"] == "include_1"
        assert ret["new_state"]["include_2"] == "include_2"
        assert ret["new_state"]["non_exists_default_val"] == "default_val"
        assert len(ret["new_state"]["eu_locations"]) == 2


def test_params_failure(hub):
    with pytest.raises(IdemRunException) as e:
        ret = run_sls(["failure"], params="params", sls_offset="sls/params")
        assert (
            "Error rendering sls: : Jinja variable 'dict object' has no attribute 'invalid'"
            == str(e)
        )


def test_params_absent():
    run_ret = run_sls(["noparams"], sls_offset="sls/params")
    assert run_ret
    assert isinstance(run_ret, dict), run_ret
    assert len(run_ret) == 3, "Expecting 3 states"
    for state, ret in run_ret.items():
        assert ret["result"] is True, state
        assert ret["new_state"]
        assert not ret["new_state"]["resource_id"]


def test_different_params_present():
    run_ret = run_sls(["noparams"], params=["params"], sls_offset="sls/params")
    assert run_ret
    assert isinstance(run_ret, dict), run_ret
    assert len(run_ret) == 3, "Expecting 3 states"
    for state, ret in run_ret.items():
        assert ret["result"] is True, state
        assert ret["new_state"]
        if ret["name"] == "vpc":
            assert ret["new_state"]["resource_id"] == "dummy"
        else:
            assert not ret["new_state"]["resource_id"]


def test_params_cli(tests_dir, idem_cli):
    sls_dir = tests_dir / "sls"
    data = idem_cli(
        "state",
        f"{sls_dir / 'params' / 'success.sls'}",
        "--param-sources",
        f"{sls_dir / 'params' / 'params.sls'}",
        "--params",
        f"{sls_dir / 'params' / 'params.sls'}",
        check=True,
    ).json
    state_id = "test_|-State Some State Present_|-State Some State Present_|-present"
    assert data[state_id]["new_state"] == {
        "backup_location": "westus",
        "empty": None,
        "empty_str": "_str",
        "include_1": "include_1",
        "include_2": "include_2",
        "location": "eastus",
        "non_exists_default_val": "default_val",
        "eu_locations": ["eu-west", "eu-north"],
    }
    assert data[state_id]["name"] == "Some State"
    assert data[state_id]["result"] is True


def test_params_cli_implicit(tests_dir, idem_cli):
    sls_dir = tests_dir / "sls"
    data = idem_cli(
        "state",
        f"{sls_dir / 'params' / 'success.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params'}",
        "--params",
        "params",
        check=True,
    ).json
    state_id = "test_|-State Some State Present_|-State Some State Present_|-present"
    assert data[state_id]["new_state"] == {
        "backup_location": "westus",
        "empty": None,
        "empty_str": "_str",
        "include_1": "include_1",
        "include_2": "include_2",
        "location": "eastus",
        "non_exists_default_val": "default_val",
        "eu_locations": ["eu-west", "eu-north"],
    }
    assert data[state_id]["name"] == "Some State"
    assert data[state_id]["result"] is True


def test_invalid_yaml_syntax_in_params(tests_dir, idem_cli):
    sls_dir = tests_dir / "sls"
    ret = idem_cli(
        "state",
        f"{sls_dir / 'params' / 'failure.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params'}",
        "--params",
        "invalid_params",
        check=False,
    ).stdout
    assert "Error while parsing" in ret
    assert "invalid_params" in ret


def test_params_override_later_include_effective():
    ret = run_sls(["params_override"], params=["params_1"], sls_offset="sls/params")
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 1
        assert ret["new_state"]["b"] == 1.2


def test_params_override_later_include_effective_reverse():
    ret = run_sls(["params_override"], params=["params_2"], sls_offset="sls/params")
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 2
        assert ret["new_state"]["b"] == 1.1


def test_params_override_multiple_files():
    ret = run_sls(
        ["params_override"],
        params=["params_1", "params_2"],
        sls_offset="sls/params",
    )
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 2
        assert ret["new_state"]["b"] == 1.1


def test_params_override_multiple_files_reverse():
    ret = run_sls(
        ["params_override"],
        params=["params_2", "params_1"],
        sls_offset="sls/params",
    )
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 1
        assert ret["new_state"]["b"] == 1.2
