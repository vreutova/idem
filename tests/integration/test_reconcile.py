import pytest
import pytest_idem.runner as runner


@pytest.mark.asyncio
async def test_no_reconcile(hub, code_dir, tmpdir):
    reconciler = "none"

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tmpdir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0


@pytest.mark.asyncio
async def test_basic_reconcile_no_reruns(hub, mock_hub, code_dir, tmpdir):
    reconciler = "basic"
    # Set up the hub like idem does

    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop

    hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name_|-state name_|-name": {
                    "changes": {},
                    "ref": "states.test.succeed_without_changes",
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "result": True,
                }
            }
        }
    }

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tmpdir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0


@pytest.mark.asyncio
async def test_basic_reconcile_exec_not_pending(hub, mock_hub, code_dir, tmpdir):
    # Skipping reconciliation for exec if the result is 'True'
    # Since exec will always have 'changes' but no need to reconcile
    reconciler = "basic"

    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop

    hub.idem.RUNS = {
        "test": {
            "running": {
                "exec_|-state name_|-state name_|-name": {
                    "changes": {},
                    "ref": "states.test.succeed_without_changes",
                    "comment": "exec.run",
                    "name": "state.name",
                    "result": True,
                }
            }
        }
    }

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tmpdir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0


@pytest.mark.asyncio
async def test_basic_reconcile_reruns(hub, mock_hub, code_dir, tmpdir):
    # Test reruns. One state require reconcile, the other does not.
    reconciler = "basic"
    sls_sources = [f"file://{code_dir}/tests/sls"]
    cache_dir = tmpdir

    mock_hub.reconcile.init.run = hub.reconcile.init.run
    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop
    mock_hub.reconcile.basic.get_pending_tags = hub.reconcile.basic.get_pending_tags
    mock_hub.reconcile.wait.static.get = hub.reconcile.wait.static.get
    mock_hub.reconcile.basic.populate_no_changes_count = (
        hub.reconcile.basic.populate_no_changes_count
    )
    mock_hub.reconcile.pending.default.is_pending = (
        hub.reconcile.pending.default.is_pending
    )
    mock_hub.idem.tools.tag_2_state = hub.idem.tools.tag_2_state
    mock_hub.states.test.succeed_without_changes = (
        hub.states.test.succeed_without_changes
    )
    mock_hub.tool.idem.arg_utils.calculate_changes = (
        hub.tool.idem.arg_utils.calculate_changes
    )
    mock_hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name1_|-state name1_|-name1": {
                    "new_state": None,
                    "old_state": None,
                    "changes": {"change": "is constant"},
                    "ref": "states.test.succeed_without_changes",
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name1",
                    "result": False,
                },
                "test.succeed_without_changes_|-state name2_|-state name2_|-name2": {
                    "new_state": None,
                    "old_state": None,
                    "changes": {},
                    "ref": "states.test.succeed_without_changes",
                    "comment": "No changes. No reconcile",
                    "name": "state.name2",
                    "result": True,
                },
            }
        }
    }

    def _check_run_init_start_params(name, pending_tags):
        assert name == "test"
        assert pending_tags
        assert (
            "test.succeed_without_changes_|-state name1_|-state name1_|-name1"
            in pending_tags
        )
        assert len(pending_tags) == 1

    mock_hub.idem.run.init.start.side_effect = _check_run_init_start_params
    ret = await mock_hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=sls_sources,
            render="yaml",
            runtime="serial",
            cache_dir=cache_dir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            acct_blob=None,
            subs=[],
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 3


@pytest.mark.asyncio
async def test_reconcile_rerun_accumulative_changes_with_metadata(
    hub, mock_hub, code_dir, tmpdir
):
    # this test verifies that the reconciliation loop updates the 'old_state'
    # In this case there is no change old_state and therefore 'changes' are not overridden.
    reconciler = "basic"
    sls_sources = [f"file://{code_dir}/tests/sls"]
    cache_dir = tmpdir

    mock_hub.reconcile.init.run = hub.reconcile.init.run
    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop
    mock_hub.reconcile.basic.get_pending_tags = hub.reconcile.basic.get_pending_tags
    mock_hub.reconcile.wait.static.get = hub.reconcile.wait.static.get
    mock_hub.reconcile.basic.populate_no_changes_count = (
        hub.reconcile.basic.populate_no_changes_count
    )
    mock_hub.reconcile.pending.default.is_pending = (
        hub.reconcile.pending.default.is_pending
    )
    mock_hub.idem.tools.tag_2_state = hub.idem.tools.tag_2_state
    mock_hub.reconcile.basic.update_result_and_send_event = (
        hub.reconcile.basic.update_result_and_send_event
    )
    mock_hub.states.test.succeed_without_changes = (
        hub.states.test.succeed_without_changes
    )
    mock_hub.tool.idem.arg_utils.calculate_changes = (
        hub.tool.idem.arg_utils.calculate_changes
    )
    mock_hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name_A_|-state name_|-name": {
                    "old_state": {"hello": "A", "world": "A"},
                    "new_state": {"hello": "A1", "world": "A1"},
                    "changes": {"change": "is constant"},
                    "ref": "states.test.succeed_without_changes",
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "rerun_data": "reconciler-metadata-A",
                    "result": True,
                },
                "test.succeed_without_changes_|-state name_B_|-state name_|-name": {
                    "old_state": {"hello": "B", "world": "B"},
                    "new_state": {"hello": "B1", "world": "B1"},
                    "changes": {"change": "is constant"},
                    "ref": "states.test.succeed_without_changes",
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "rerun_data": "reconciler-metadata-B",
                    "result": True,
                },
            }
        }
    }

    def _check_run_init_start_params(name, pending_tags):
        assert name == "test"
        assert pending_tags
        assert len(pending_tags) == 2

    mock_hub.idem.run.init.start.side_effect = _check_run_init_start_params
    ret = await mock_hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            subs=["states"],
            sls_sources=sls_sources,
            render="yaml",
            runtime="serial",
            cache_dir=cache_dir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_blob=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 3
    run_state_A = mock_hub.idem.RUNS["test"]["running"][
        "test.succeed_without_changes_|-state name_A_|-state name_|-name"
    ]
    run_state_B = mock_hub.idem.RUNS["test"]["running"][
        "test.succeed_without_changes_|-state name_B_|-state name_|-name"
    ]
    assert run_state_A
    assert run_state_A["changes"]
    assert run_state_B["changes"]
    # Change is calculated by the reconciliation loop
    assert {
        "new": {"hello": "A1", "world": "A1"},
        "old": {"hello": "A", "world": "A"},
    } == run_state_A["changes"]
    assert {
        "new": {"hello": "B1", "world": "B1"},
        "old": {"hello": "B", "world": "B"},
    } == run_state_B["changes"]

    assert run_state_A["rerun_data"] == "reconciler-metadata-A"
    assert run_state_B["rerun_data"] == "reconciler-metadata-B"


def test_populate_comments(hub):
    # Check run with comments of different types
    run_1 = {
        "tag1": {"comment": "str_comment"},
        "tag2": {"comment": ["list_comment_elem1", "list_comment_elem2"]},
        "tag3": {
            "comment": (
                "tuple_elem1",
                "tuple_elem2",
                "tuple_elem3",
            )
        },
        "tag4": {"comment": {"dic_comment": "dic_value"}},
    }

    tag_to_comments = {}
    tag_to_comments = hub.reconcile.basic.populate_comments(run_1, tag_to_comments)
    assert tag_to_comments
    assert tag_to_comments.get("tag1") == ("str_comment",)
    assert tag_to_comments.get("tag2") == (
        "list_comment_elem1",
        "list_comment_elem2",
    )
    assert tag_to_comments.get("tag3") == (
        "tuple_elem1",
        "tuple_elem2",
        "tuple_elem3",
    )
    assert not tag_to_comments.get("tag4")

    # Concatenate a second run
    run_2 = {
        "tag1": {"comment": {"dic_comment": "dic_value"}},
        "tag2": {
            "comment": (
                "tuple_elem1",
                "tuple_elem2",
                "tuple_elem3",
            )
        },
        "tag3": {"comment": ["list_comment_elem1", "list_comment_elem2"]},
        "tag4": {"comment": "str_comment"},
    }
    tag_to_comments = hub.reconcile.basic.populate_comments(run_2, tag_to_comments)
    assert tag_to_comments.get("tag1") == ("str_comment",)
    assert tag_to_comments.get("tag2") == (
        "list_comment_elem1",
        "list_comment_elem2",
        "tuple_elem1",
        "tuple_elem2",
        "tuple_elem3",
    )
    assert tag_to_comments.get("tag3") == (
        "tuple_elem1",
        "tuple_elem2",
        "tuple_elem3",
        "list_comment_elem1",
        "list_comment_elem2",
    )
    assert tag_to_comments.get("tag4") == ("str_comment",)


def test_reconcile_with_dependencies(idem_cli, tests_dir, mock_time):
    # Test reconciliation where the pending resource has a dependency ('require' or 'arg_bind').
    # Since the required is not a resource in ESM it will be re-executed as well.
    # Re-runs due to reconciliation are verified by concatenated 'rerun_data'.
    output = idem_cli(
        "state", tests_dir / "sls" / "reconcile" / "reconcile.sls", check=True
    ).json

    assert 5 == len(output), len(output)
    # succeed_without_changes_with_rerun_data concatenates `WO_RERUN-` to rerun_data for each execution
    assert (
        output["test_|-good_|-good_|-succeed_without_changes_with_rerun_data"]["result"]
        is True
    )
    assert (
        output["test_|-good_|-good_|-succeed_without_changes_with_rerun_data"].get(
            "rerun_data"
        )
        is not None
    )
    # succeed_without_changes_with_rerun_data has "rerun_data", so it will be re-run until max-runs without changes
    assert (
        output["test_|-good_|-good_|-succeed_without_changes_with_rerun_data"][
            "rerun_data"
        ]
        == "WO_RERUN-WO_RERUN-WO_RERUN-WO_RERUN-"
    )

    # succeed_with_changes_with_rerun_data concatenates `W_RERUN-` to rerun_data for each execution
    assert (
        output["test_|-change_1_|-change_1_|-succeed_with_changes_with_rerun_data"][
            "result"
        ]
        is True
    )
    assert (
        output["test_|-change_1_|-change_1_|-succeed_with_changes_with_rerun_data"].get(
            "rerun_data"
        )
        is not None
    )
    assert (
        output["test_|-change_1_|-change_1_|-succeed_with_changes_with_rerun_data"][
            "rerun_data"
        ]
        == "W_RERUN-W_RERUN-W_RERUN-W_RERUN-"
    )

    # succeed_with_changes_with_rerun_data concatenates `WO_RERUN-` to rerun_data for each execution
    assert (
        output[
            "test_|-first thing_|-first thing_|-succeed_with_changes_with_rerun_data"
        ]["result"]
        is True
    )
    assert (
        output[
            "test_|-first thing_|-first thing_|-succeed_with_changes_with_rerun_data"
        ].get("rerun_data")
        is not None
    )
    assert (
        output[
            "test_|-first thing_|-first thing_|-succeed_with_changes_with_rerun_data"
        ]["rerun_data"]
        == "W_RERUN-W_RERUN-W_RERUN-W_RERUN-"
    )

    # succeed_with_arg_bind has no rerun_data
    assert (
        output["test_|-change_2_|-change_2_|-succeed_with_arg_bind"]["result"] is True
    )
    assert (
        output["test_|-change_2_|-change_2_|-succeed_with_arg_bind"].get("rerun_data")
        is None
    )

    assert (
        output["test_|-change_3_|-change_3_|-succeed_with_arg_bind"]["result"] is True
    )
    assert (
        output["test_|-change_3_|-change_3_|-succeed_with_arg_bind"].get("rerun_data")
        is None
    )


def test_reconcile_with_exec_and_rerun_data(idem_cli, tests_dir, mock_time):
    # Test reconciliation of exec.run
    # For reconciliation of exec.run the result should be 'False'
    # Re-runs due to reconciliation are verified by exection_count
    output = idem_cli(
        "state", tests_dir / "sls" / "reconcile" / "reconcile_exec.sls", check=True
    ).json

    assert 1 == len(output), len(output)
    assert output["exec_|-exec_with_rerun_|-exec_with_rerun_|-run"]["result"] is True
    assert (
        output["exec_|-exec_with_rerun_|-exec_with_rerun_|-run"].get("rerun_data")
        is not None
    )
    assert (
        output["exec_|-exec_with_rerun_|-exec_with_rerun_|-run"]["rerun_data"][
            "execution_count"
        ]
        == 3
    )


def test_reconcile_with_custom_is_pending_1(idem_cli, tests_dir, mock_time):
    # Test reconciliation with custom is_pending implementation, with this signature:
    # def is_pending(hub, ret: dict, state: str = None, **pending_kwargs)
    # Will stop after 'max-pending-reruns' as specified by CLI
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_custom_is_pending_signature_1.sls",
        "--max-pending-reruns=4",
        check=True,
    ).json

    assert 2 == len(output), len(output)
    # Stopped based on the defined 'number_of_reruns' in the resource
    tag1 = "reconcile.is_pending.resource_with_is_pending_signature_1_|-test_1_|-test_1_|-present"
    assert output[tag1]["result"] is True
    assert output[tag1].get("rerun_data") is not None
    assert output[tag1]["rerun_data"]["execution_count"] == 3

    # Stopped based on 'max-pending-reruns' specified in CLI (4 reruns)
    tag2 = "reconcile.is_pending.resource_with_is_pending_signature_1_|-test_2_|-test_2_|-present"
    assert output[tag2]["result"] is True
    assert output[tag2].get("rerun_data") is not None
    assert output[tag2]["rerun_data"]["execution_count"] == 5


def test_reconcile_max_pending_reruns(idem_cli, tests_dir, mock_time):
    # Test reconciliation stops after 'max-pending-reruns' without customized 'is_pending'
    # if the 'changes' are not constant (i.e.: keeps changing)
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_always_pending.sls",
        "--max-pending-reruns=4",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    tag = "test_|-always_pending_|-always_pending_|-succeed_with_non_constant_changes_with_rerun_data"
    assert output[tag]["result"] is True
    assert output[tag].get("rerun_data") is not None
    # Verify it ran with 4 reruns, total 5 times as specified by max-pending-reruns
    assert output[tag]["rerun_data"] == "W_RERUN-W_RERUN-W_RERUN-W_RERUN-W_RERUN-"


def test_reconcile_with_custom_is_pending_2(idem_cli, tests_dir, mock_time):
    # Test reconciliation with custom is_pending implementation, with this signature:
    # is_pending(hub, ret: dict)
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_custom_is_pending_signature_2.sls",
        check=True,
    ).json

    tag1 = "reconcile.is_pending.resource_with_is_pending_signature_2_|-test_1_|-test_1_|-present"
    assert output[tag1]["result"] is True
    assert output[tag1].get("rerun_data") is not None
    assert output[tag1]["rerun_data"]["execution_count"] == 3


def test_reconcile_with_custom_is_pending_and_state_failure_and_no_change(
    idem_cli, tests_dir, mock_time
):
    """
    Test reconciliation with custom is_pending and state execution failure
    Expectation: Reconciliation should come out with
    """
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_custom_is_pending_signature_2.sls",
        check=True,
    ).json

    tag1 = "reconcile.is_pending.resource_with_is_pending_signature_2_|-test_1_|-test_1_|-present"
    assert output[tag1]["result"] is True
    assert output[tag1].get("rerun_data") is not None
    assert output[tag1]["rerun_data"]["execution_count"] == 3


def test_reconcile_with_custom_is_pending_always_pending(
    idem_cli, tests_dir, mock_time
):
    output = idem_cli(
        "state",
        tests_dir
        / "sls"
        / "reconcile"
        / "reconcile_custom_is_pending_and_always_pending.sls",
        "--max-pending-reruns=4",
        check=True,
    ).json

    assert 3 == len(output), len(output)
    tag = "reconcile.is_pending.resource_with_success_and_always_pending_|-test_1_|-test_1_|-present"
    assert output[tag]["result"] is True
    assert output[tag].get("rerun_data") is not None
    assert output[tag].get("rerun_data").get("execution_count") is not None
    # Verify it ran with 5 runs as defined by (1 actual + --max-pending-reruns re-runs)
    assert output[tag].get("rerun_data").get("execution_count") == 5

    tag = "reconcile.is_pending.resource_with_failure_and_always_pending_|-test_2_|-test_2_|-present"
    assert output[tag]["result"] is False
    assert output[tag].get("rerun_data") is not None
    assert output[tag].get("rerun_data").get("execution_count") is not None
    # Verify it ran with 4 runs as defined by (1 actual + MAX_RERUNS_WO_CHANGE re-runs)
    assert output[tag].get("rerun_data").get("execution_count") == 4

    tag = "reconcile.is_pending.resource_with_pending_and_overridden_empty_changes_|-test_3_|-test_3_|-present"
    assert output[tag]["result"] is False
    assert output[tag].get("rerun_data") is not None
    assert output[tag].get("rerun_data").get("execution_count") is not None
    # Verify it ran with 3 runs as defined by (1 actual + 2 re-runs without change as defined by custom is_pending)
    assert output[tag].get("rerun_data").get("execution_count") == 3
    assert output[tag].get("old_state") == {
        "irrelevant_prop": -1,
        "relevant_prop": "relevant_value",
    }
    assert output[tag].get("new_state") == {
        "irrelevant_prop": 3,
        "relevant_prop": "relevant_value",
    }
    assert output[tag].get("changes") == {
        "old": {"irrelevant_prop": -1},
        "new": {"irrelevant_prop": 3},
    }


def test_reconcile_with_clearing_rerun_data(idem_cli, tests_dir, mock_time):
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_clearing_rerun_data.sls",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    tag = "reconcile.resource_with_clearing_rerun_data_|-test_1_|-test_1_|-present"
    assert output[tag]["result"] is True
    assert output[tag].get("rerun_data") is None
    assert output[tag].get("new_state") is not None
    assert output[tag].get("new_state").get("execution_count") == 2


def test_reconcile_with_rerun_data_always_present_success(
    idem_cli, tests_dir, mock_time
):
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_with_rerun_data_success.sls",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    tag = "reconcile.resource_with_rerun_data_success_|-test_1_|-test_1_|-present"
    assert output[tag]["result"] is True
    assert output[tag].get("rerun_data") is not None
    assert output[tag].get("rerun_data").get("execution_count") is not None
    # Verify it ran with 4 runs as defined by (1 actual + MAX_RERUNS_WO_CHANGE re-runs)
    assert output[tag].get("rerun_data").get("execution_count") == 4


def test_reconcile_with_rerun_data_always_present_failure(
    idem_cli, tests_dir, mock_time
):
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_with_rerun_data_failure.sls",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    tag = "reconcile.resource_with_rerun_data_failure_|-test_1_|-test_1_|-present"
    assert output[tag]["result"] is False
    assert output[tag].get("rerun_data") is not None
    assert output[tag].get("rerun_data").get("execution_count") is not None
    # Verify it ran with 4 runs as defined by (1 actual + MAX_RERUNS_WO_CHANGE re-runs)
    assert output[tag].get("rerun_data").get("execution_count") == 4


def test_reconcile_with_result_false_then_true(idem_cli, tests_dir, mock_time):
    # result is False twice, and then True
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_clearing_changes.sls",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    tag = "reconcile.resource_with_clearing_changes_|-test_1_|-test_1_|-present"
    assert output[tag]["result"] is True
    assert output[tag].get("rerun_data") is None
    assert output[tag].get("new_state") is not None
    # After 2 runs, the result changes to True
    assert output[tag].get("new_state").get("execution_count") == 2


def test_reconcile_without_clearing_changes(idem_cli, tests_dir, mock_time):
    # result if 'False' to trigger reconciliation.
    # Stop after 5 re-runs
    output = idem_cli(
        "state",
        tests_dir / "sls" / "reconcile" / "reconcile_changes_always_present.sls",
        "--max-pending-reruns=4",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    tag = "reconcile.resource_with_changes_always_present_|-test_1_|-test_1_|-present"
    assert output[tag]["result"] is False
    assert output[tag].get("rerun_data") is None
    assert output[tag].get("new_state") is not None
    # Verify it ran with 5 runs (1 actual + --max-pending-reruns)
    assert output[tag].get("new_state").get("execution_count") == 5


def test_reconcile_with_func_not_found_state(idem_cli, tests_dir, mock_time):
    acct_data = {"profiles": {"test": {"default": {}}}}
    # This issue happens when the function in SLS is not found
    resources = """
        test_resource:
            non_existing.function:
              - hello: world
        """
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(resources)
        return idem_cli(
            "state",
            fh,
            "--run-name=test",
            check=True,
            acct_data=acct_data,
        ).json
    assert "test_resource_|-test_resource_|-test_resource_name_|-present" in ret

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_resource_name_|-"] == {
            "resource_id": "resource123"
        }
