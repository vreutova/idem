import pathlib

import msgpack
import pytest_idem.runner as runner
from pytest_idem.runner import idem_cli


CREATE_RESOURCE_IN_ESM = """
test_resource:
    test_resource.present:
      - name: test_1
      - changes: {}
      - old_state: {resource_id: resource123}
      - new_state: {resource_id: resource123}
      - skip_esm: False
"""

RECREATE_RESOURCE_WITH_RECONCILE = """
test_resource:
    test_resource.present:
      - name: test_1
      - recreate_resource_without_resource_id: true
      - skip_esm: False
      - force_save: True
"""


def test_recreate_if_deleted(tmpdir, tests_dir):
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    # Create resource in ESM
    execute_sls(cache_dir, CREATE_RESOURCE_IN_ESM)

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_resource_|-"] == {
            "resource_id": "resource123"
        }

    # Resource re-enforcement fails
    # resource_id is cleared from ESM
    # Resource re-enforcement succeeds during next run of reconciliation
    output = execute_sls(cache_dir, RECREATE_RESOURCE_WITH_RECONCILE)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_resource_|-present"
    # Result is True
    assert output[tag]["result"] is True
    # Executed 2 times
    assert output[tag].get("__run_num") == 2

    # No resource_id in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data.get("test_resource_|-test_resource_|-test_resource_|-", {}) == {}


def test_recreate_if_deleted_in_test_mode(tmpdir, tests_dir):
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    # Create resource in ESM
    execute_sls(cache_dir, CREATE_RESOURCE_IN_ESM)

    assert esm_cache.exists()
    # resource_id is stored in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_resource_|-"] == {
            "resource_id": "resource123"
        }

    # Resource re-enforcement fails
    # resource_id is cleared from ESM
    # Resource re-enforcement succeeds during next run of reconciliation
    output = execute_sls(cache_dir, RECREATE_RESOURCE_WITH_RECONCILE, True)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_resource_|-present"
    # Result is True
    assert output[tag]["result"] is True
    # Executed 1 times - no reconciliation in the test mode
    assert output[tag].get("__run_num") == 1

    # resource_id in ESM
    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        assert data["test_resource_|-test_resource_|-test_resource_|-"] == {
            "resource_id": "resource123"
        }


def execute_sls(cache_dir, sls_to_execute, test=False):
    acct_data = {"profiles": {"test": {"default": {}}}}
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(sls_to_execute)

        if test:
            return idem_cli(
                "state",
                fh,
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--esm-plugin=local",
                "--run-name=test",
                f"--test",
                check=True,
                acct_data=acct_data,
            ).json
        else:
            return idem_cli(
                "state",
                fh,
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--esm-plugin=local",
                "--run-name=test",
                check=True,
                acct_data=acct_data,
            ).json
