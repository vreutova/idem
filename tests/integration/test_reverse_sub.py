import pop.hub
import pop.loader
import pytest_idem.runner as runner


def test_contracts_separated_resolver(capsys, hub):
    # Set up the hub like idem does
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    runner.patch_hub(hub)
    hub.pop.loop.create()

    def resolve(path, context):
        return hub.exec.tests.ctx.get

    hub.pop.sub.dynamic(resolve, None, dyne_name="mod", sub=hub.exec)

    ret = hub.pop.Loop.run_until_complete(
        hub.idem.ex.run("exec.mod.foo.bar", args=[], kwargs={})
    )
    assert ret.result
    assert ret.ret == {"acct": {}, "acct_details": {}, "test": False}
    assert ret.ref == "mod.foo.bar.get"
