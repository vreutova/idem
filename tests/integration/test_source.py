def test_multiple_sls(tests_dir, mock_time, idem_cli):
    output = idem_cli(
        "state",
        tests_dir / "sls" / "noop.sls",
        tests_dir / "sls" / "noop2.sls",
        check=True,
    ).json

    output["test_|-First State_|-First State_|-nop"].pop("start_time")
    output["test_|-First State_|-First State_|-nop"].pop("total_seconds")
    assert output["test_|-First State_|-First State_|-nop"] == {
        "__run_num": 1,
        "changes": {},
        "comment": ["Success!"],
        "esm_tag": "test_|-First " "State_|-First State_|-",
        "name": "First State",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "__id__": "First State",
        "tag": "test_|-First State_|-First " "State_|-nop",
    }
    output["test_|-Second State_|-Second State_|-nop"].pop("start_time")
    output["test_|-Second State_|-Second State_|-nop"].pop("total_seconds")
    assert output["test_|-Second State_|-Second State_|-nop"] == {
        "__run_num": 2,
        "changes": {},
        "comment": ["Success!"],
        "esm_tag": "test_|-Second " "State_|-Second State_|-",
        "name": "Second State",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "__id__": "Second State",
        "tag": "test_|-Second State_|-Second " "State_|-nop",
    }
