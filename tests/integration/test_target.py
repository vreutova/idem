def test_single_target(idem_cli, tests_dir, mock_time):
    # Specify a target resource,
    # only the target and its requisites should be invoked,
    # in this case the target and 2 dependent resources.
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=the_target",
        check=True,
    ).json

    assert 3 == len(output), len(output)
    output["test_|-dep_2_|-dep_2_|-nop"].pop("start_time")
    output["test_|-dep_2_|-dep_2_|-nop"].pop("total_seconds")
    assert output["test_|-dep_2_|-dep_2_|-nop"] == {
        "__run_num": 1,
        "changes": {},
        "comment": ["Success!"],
        "esm_tag": "test_|-dep_2_|-dep_2_|-",
        "name": "dep_2",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "__id__": "dep_2",
        "tag": "test_|-dep_2_|-dep_2_|-nop",
    }

    output["test_|-dep_1_|-dep_1_|-nop"].pop("start_time")
    output["test_|-dep_1_|-dep_1_|-nop"].pop("total_seconds")
    assert output["test_|-dep_1_|-dep_1_|-nop"] == {
        "__run_num": 2,
        "changes": {},
        "comment": ["Success!"],
        "esm_tag": "test_|-dep_1_|-dep_1_|-",
        "name": "dep_1",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "__id__": "dep_1",
        "tag": "test_|-dep_1_|-dep_1_|-nop",
    }

    output["test_|-the_target_|-the_target_|-succeed_without_changes"].pop("start_time")
    output["test_|-the_target_|-the_target_|-succeed_without_changes"].pop(
        "total_seconds"
    )
    assert output["test_|-the_target_|-the_target_|-succeed_without_changes"] == {
        "__run_num": 3,
        "changes": {},
        "comment": ["Success!"],
        "esm_tag": "test_|-the_target_|-the_target_|-",
        "name": "the_target",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "__id__": "the_target",
        "tag": "test_|-the_target_|-the_target_|-succeed_without_changes",
    }


def test_target_with_arg_bind(idem_cli, tests_dir, mock_time):
    # Testing 'target' with arg_bind to a different resource
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=target_w_arg_bind_codependent",
        "--reconciler=none",
        check=True,
    ).json

    assert 2 == len(output), len(output)
    tag1 = "test_|-target_w_arg_bind_codependent_|-target_w_arg_bind_codependent_|-succeed_with_arg_bind"
    tag2 = "test_|-independent_target_|-independent_target_|-succeed_with_arg_bind"
    assert output[tag1]
    assert output[tag2]
    assert output[tag1]["result"]
    assert output[tag2]["result"]

    assert output[tag1]["new_state"]["testing"] == output[tag2]["new_state"]["testing"]


def test_target_negative(idem_cli, tests_dir, mock_time):
    # Specify the name of the resource, which is different from the
    # property name. It should fail.
    ret = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=the_name",
        "--output=state",
        check=False,
    ).stdout
    assert "Invalid 'target' for run 'cli': the_name." in ret


def test_nested_resources(idem_cli, tests_dir, mock_time):
    # Target is a declaration ID that has multiple nested resources
    # All will be invoked with their dependencies
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=nested_resources",
        check=True,
    ).json

    assert 4 == len(output), len(output)
    assert output.get("test_|-nested_resources_|-test_nested_|-succeed_without_changes")
    assert output.get("test_|-dep_2_|-dep_2_|-nop")
    assert output.get("test_|-dep_1_|-dep_1_|-nop")
    assert output.get(
        "test_regex_|-nested_resources_|-nested_resources_|-none_without_changes_regex"
    )


def test_target_with_arg_bind_from_esm(idem_cli, tests_dir, mock_time):
    # First time the SLS with 2 resources is executed to populate the ESM.
    # Second time same SLS with --target a resource with arg bind to
    # a resource in ESM that has a tag with 'name_prefix' value.
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target_with_name_prefix.sls",
        "--esm-plugin=local",
        "--reconcile=none",
        check=True,
    ).json

    assert 2 == len(output), len(output)
    for tag in output:
        assert output[tag]["result"] is True, tag

    # Now invoke with --target, with arg_bind to an object in the ESM
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target_with_name_prefix.sls",
        "--target=test-for-target",
        "--esm-plugin=local",
        "--reconcile=none",
        check=True,
    ).json

    assert 1 == len(output), len(output)
    assert (
        output["test_|-test-for-target_|-depends-on-idem-test_|-present"]["result"]
        is True
    )
