first thing:
  test.succeed_with_changes

arg bind ref:
  test.succeed_with_arg_bind:
    - parameters:
        test1: First - ${test:first thing:testing:new_param}. Second - ${test:first thing:tests[0][0]:new_param}. Finished
        test2: ${test:first thing:tests[0][0]:new_param}
        test3:
          - ${test:third thing:testing:new_param}
          - ${test:third thing:tests[0][0]:new_param} -- ${test:third thing:tests[0][0]:new_param}
        test4: ${test:first thing:new_param}

third thing:
  test.succeed_with_changes
