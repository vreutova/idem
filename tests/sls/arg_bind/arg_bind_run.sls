state_A:
  test.present:
    - new_state:
        key: state_a_name

state_B:
  test.present:
    - resource_name: state_b_name
    - new_state:
        key: idem-test-${test:state_A:key}
