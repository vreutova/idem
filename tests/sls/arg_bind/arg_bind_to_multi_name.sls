# Use arg bind with a specific resource name
test_bind_1:
  test.present:
    - result: True
    - new_state:
        key: binding
        name: bind_to_3_resources_different_names
        tags:
           tag1: ${test:esm_test_state[esm test state 1]:value}
           tag2: ${test:esm_test_state[esm test state 2]:value}
           tag3: ${test:esm_test_state[esm test state 3]:value}
           tag4: ${test:esm_test_state[esm test state 4]:value}


# Only the 'esm test state 4' is here the other 3 were created before
esm_test_state:
  test.present:
    - resource_name: esm test state 4
    - result: True
    - new_state:
        value: esm_test_value_4
        name: esm test state 4
