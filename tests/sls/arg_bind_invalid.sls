test_1:
    test_resource.present:
        - name: test_1
        - group: group_1

test_3_invalid_arg_bind_format:
    test_resource.present:
        - name: test_3
        - group: ${test_resource.present:test_1:group}
