succeed_with_changes_state_2:
  test.succeed_with_changes:
    - resource_name: succeed_with_changes_state_2

#!require:succeed_with_changes_state_2

{% set nested_value = hub.idem.arg_bind.resolve("${test:succeed_with_changes_state_2:testing:new}") %}
test_arg_bind_jinja_2:
  test.succeed_with_comment:
    - comment: {{ nested_value }}
#!END
