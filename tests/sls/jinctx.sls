{% set ctx = hub.idem.acct.ctx("test", profile="default") %}
test_implicit_ctx:
  test.succeed_with_comment:
    - comment: {{ hub.exec.tests.ctx.get(ctx).ret }}

{% set ctx = hub.idem.acct.ctx("exec.test.*", profile="default") %}
test_ctx_minimal:
  test.succeed_with_comment:
    - comment: {{ hub.exec.tests.ctx.get(ctx).ret }}

{% set ctx = hub.idem.acct.ctx("exec.test.*", profile="default") %}
test_ctx_explicit_full_path:
  test.succeed_with_comment:
    - comment: {{ hub.exec.tests.ctx.get(ctx=ctx).ret }}

{% set ctx = hub.idem.acct.ctx("my_cloud", "my_profile") %}
test_minimal_ctx:
  test.succeed_with_comment:
    - comment: {{ hub.exec.tests.ctx.get(ctx).ret }}
