test-name-differ-from-esm-tag:
  test.present:
    - resource_name: idem-test-name
    - name_prefix: name-for-esm-tag
    - new_state:
        key: value
        name: idem-test-name
        resource_id: idem-test-1
        name_prefix: name-for-esm-tag
    - result: true

test-for-target:
   test.present:
     - resource_name: depends-on-idem-test
     - tags:
        key: ${test:test-name-differ-from-esm-tag:name}
     - result: true
