template_render_with_template_file_name:
  template.render:
    - resource_name: template_render_test-1
    - template_file_name: "./tests/sls/templates/hello.tpl"
    - variables:
        name: "test-idem"

template_render_with_template:
  template.render:
    - resource_name: template_render_test-2
    - template: '{% raw %}Hello {{ name }} !!{% endraw %}'
    - variables:
        name: "test-idem"

template_render_with_wrong_template_file_name:
  template.render:
    - resource_name: template_render_test-3
    - template_file_name: "./tests/sls/templates/abc.tpl"
    - variables:
        name: "test-idem"

template_render_when_both_template_file_name_and_template_is_none:
  template.render:
    - resource_name: template_render_test-4

template_render_with_template_file_name_but_no_variables:
  template.render:
    - resource_name: template_render_test-5
    - template_file_name: "./tests/sls/templates/hello-world.tpl"

template_render_with_template_but_no_variables:
  template.render:
    - resource_name: template_render_test-6
    - template: '{% raw %}Hello World !!{% endraw %}'
