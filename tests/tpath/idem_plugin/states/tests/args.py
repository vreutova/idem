"""
This plugin existing tests the signature of the resource contract
"""
__contracts__ = ["resource"]


def present(hub, ctx, name: str, required):
    ...


def absent(hub, ctx, name: str, required):
    ...


async def describe(hub, ctx):
    ...
